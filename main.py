from typing import List, Dict

import numpy as np
import networkx as nx
import random
import sys
import time

from test_constants import *


def make_test(nodes: int, edges: int) -> (Dict, Dict):
    """
    Funkcja przeprowadzająca test dla par sieci
    :param nodes:
    :param edges:
    """
    
    # Utworzenie słowników z pustymi listami na wyniki
    all_results_for_networks_ba = {key: [] for key in range(BASE_ATTACK_STRENGTH, FINAL_ATTACK_STRENGTH + 1)}
    all_results_for_networks_er = {key: [] for key in range(BASE_ATTACK_STRENGTH, FINAL_ATTACK_STRENGTH + 1)}
    
    # Utworzenie par sieci
    for network_pair in range(NETWORK_PAIRS_TO_TEST):
        net_ba, net_er = make_networks(nodes=nodes, edges=edges)
        
        # Przeprowadzenie ataków dla siły ataku określonego parametrami
        for attack_strength in range(BASE_ATTACK_STRENGTH, FINAL_ATTACK_STRENGTH + 1):
            one_attack_results_ba = attack_network(network=net_ba, attack_strength=attack_strength)
            one_attack_results_er = attack_network(network=net_er, attack_strength=attack_strength)
            
            # Dopisanie wyników do słownika do odpowiedniego klucza odpowiadającego sile ataku
            all_results_for_networks_ba[attack_strength].extend(one_attack_results_ba)
            all_results_for_networks_er[attack_strength].extend(one_attack_results_er)

    return all_results_for_networks_ba, all_results_for_networks_er


def make_networks(nodes: int, edges: int) -> (nx.Graph, nx.Graph):
    """
    Funkcja tworząca parę sieci B-A oraz E-R
    :param nodes:
    :param edges:
    """
    
    # Obliczenie prawdopodobieństwa powstania krawędzi w grafie Edros-Renyi
    epsilon = (2*edges)/(nodes-1)
    
    # Generacja grafów
    net_ba = nx.barabasi_albert_graph(nodes, edges)
    net_er = nx.erdos_renyi_graph(nodes, epsilon)
    
    # Sprawdzanie spójności
    valid_ba = check_if_network_is_valid(network=net_ba)
    valid_er = check_if_network_is_valid(network=net_er)

    return valid_ba, valid_er


def check_if_network_is_valid(network: nx.Graph) -> nx.Graph:
    """
    Funkcja sprawdzająca czy sieć jest spójna,
    W przypadku gdy jest niespójna z sieci zostaną usunięte wyizolowane
        wierzchołki
    :param network:
    """
    
    # Sprawdzenie spójności
    if nx.is_connected(network):
        print('NETWORK VALID')
        return network
    else:
        # Tworzenie podgrafu z największego komponektu sieci, sprowadza się do pozostawiania 
        #     największego komponentu i usunięcie pozostałych mniejszych w tym wyizolowanych wierzchołków
        connected_network = network.subgraph(
            max(nx.connected_components(network), key=len)
        ).copy() 
        print(f'NETWORK INVALID:{network.number_of_nodes()} | {connected_network.number_of_nodes()}')
        return connected_network


def attack_network(network: nx.Graph, attack_strength: int) -> List:
    """
    Funkcja przeprowadza atak na graf z podaną siłą
    :param network:
    :param attack_strength:
    """
    # Inicjalizacja tablicy na wyniki
    attacks_results = []

    for attack in range(MAX_ATTACKS):
        
        # Sprawdzenie czy została osiągnięta maksymalna ilość rozspojeń sieci
        if sum(attacks_results) == MAX_SUCCESSFUL_ATTACKS:
            return attacks_results
        
        # Obliczenie ile wierzchołków należy usunąć w zależności od siły ataku,
        #     siła ataku = procent wierzchołków do usunięcia
        attacked_nodes = int(network.number_of_nodes() * (attack_strength/100))
        
        # Losowe wybranie obliczonej powyżej ilość wierzchołków do usunięcia z grafu
        deleted_nodes = random.sample(network.nodes, attacked_nodes)
        
        # Utworzenie kopii sieci
        network_to_attack = network.copy()
        
        # Usunięcie wybranych wierzchołków z sieci
        for deleted_node in deleted_nodes:
            network_to_attack.remove_node(deleted_node)
        
        # Sprawdzenie spójności sieci
        integrity = check_integrity(network=network_to_attack)
        
        # Dopisanie wykiku funkcji check_integrity do listy
        attacks_results.append(integrity)

    return attacks_results


def check_integrity(network: nx.Graph) -> int:
    """
    Sprawdza spójność sieci, jeżeli sieć jest spójna zostanie zwrócone 0,
        jeżeli sieć jest niespójna zostanie zwrócona 1
    :param network:
    """
    if nx.is_connected(network):
        return 0
    else:
        return 1


def average_results_and_save(net_results: Dict, file_name: str) -> None:
    """
    Funkcja oblicza prawdopodobieństwo wystapienia rozspójnienia sieci 
        oraz zapisuje do pliku csv wynik prawdopodobieństwa dla każdej siły ataku
        
    Prawdopodobieństwo = średnia z wyników dla jednej siły ataku        
    :param net_results:
    :param file_name:
    """
    # Utworzenie słownika {siła_ataku: prawdopodobieństwo} i wyliczenie prawdopodobieństwa
    final_results = {attack_strength: np.array(results).mean() for attack_strength, results in net_results.items()}
    
    # zapis do pliku
    with open(f'{file_name}.csv', 'w') as opened_file:
        for attack_strength, result in final_results.items():
            opened_file.write(f'{attack_strength},{result}\n')


def main() -> None:
    """
    Głowna funkcja projektu
    
    Poniższe parametry programu znajdują się w pliku test_constants.py

    NODES - ilość wierzchołków w grafie
    EDGES - ilość krawędzi w grafie
    BASE_ATTACK_STRENGTH - minimalna siła ataku
    FINAL_ATTACK_STRENGTH - maksymalna siła ataku
    NETWORK_PAIRS_TO_TEST - ilość par sieci do utworzenia
    MAX_ATTACKS - ilość prób ataków przy jednej sile ataku
    MAX_SUCCESSFUL_ATTACKS - maksymalna liczba sukcesów rozspójnienia sieci
    """
    # Ustawienie ziarna na obecną chwilę
    random.seed(time.time())
    # Przeprowadzenie testu
    ba_results, er_results = make_test(nodes=NODES, edges=EDGES)
    
    # Wyliczenie prawdopodobieństwa oraz zapis wyników do pliku
    average_results_and_save(ba_results, 'ba_results')
    average_results_and_save(er_results, 'er_results')


if __name__ == "__main__":
    sys.exit(main() or 0)